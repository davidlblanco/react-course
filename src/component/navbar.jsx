import React, { Component } from "react";
class Navbar extends Component {
  render() {
    return (
      <nav className="navbar navbar-light bg-light">
        <a className="navbar-brand" href="#">
          <span className="badge badge-pill badge-primary mr-2">
            {this.totalSum()}
          </span>
          Navbar
        </a>
      </nav>
    );
  }
  totalSum() {
    let total = 0;
    this.props.counters.map((counter) => {
      total = total + counter.value;
    });
    return total;
  }
}

export default Navbar;
